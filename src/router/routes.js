const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: '', component: () => import('pages/Home.vue'), name: 'root' },
      { path: 'register', component: () => import('components/User/Register.vue') },
      { path: 'login', component: () => import('components/User/Login.vue'), name: 'login' }
    ]
  },
  {
    path: '/user/:id/',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '', component: () => import('pages/User.vue'), name: 'user' },
      { path: 'profile', component: () => import('components/User/Profile.vue') },
      { path: 'dashboard', component: () => import('components/Dashboard/Dashboard.vue') },
      { path: 'newaccount', component: () => import('components/Account/NewAccount.vue') },
      // voir s'il ne faut pas rajouter un sous-enfant
      { path: 'addmvmt', component: () => import('components/Account/AddMouvment.vue'), name: 'addMvmt' },
      { path: 'period', component: () => import('components/Account/AccountStatPeriod.vue') }
    ]
  }
]
// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
