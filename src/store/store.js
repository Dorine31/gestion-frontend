export default {
  name: '',
  email: '',
  password: '',
  errors: [],
  fullObjAuthUser: null,
  isAuth: false,
  url: 'http://localhost:3001',
  token: '',
  id: 0,
  // account
  accountName: '',
  bankName: '',
  accountNumber: null,
  balance: null,
  dateBalance: null,
  comptes: [],
  idSet: new Set(),
  // authlayout
  leftDrawerOpen: false,
  path: 'https://cdn.pixabay.com/photo/2014/04/02/17/07/user-307993_960_720.png',
  // add mvment
  options: ['Créditer', 'Débiter'],
  mvmtOptions: '',
  labelMvmt: '',
  amount: 0,
  dateMvmt: null,
  idAccount: 0,
  // periode statement
  start: null,
  end: null,
  movments: []
}
